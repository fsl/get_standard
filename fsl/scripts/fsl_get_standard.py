#!/usr/bin/env python
#
# fsl_get_standard.py - Generate paths to FSL standard space images.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#
"""The fsl_get_standard command can be used to locate standard template files,
e.g. $FSLDIR/data/standard/MNI152_T1_2mm.nii.gz.  It is intended to be used by
tools which can be used with standard templates other than the MNI152.

"""


import functools as ft
import os.path   as op
import              os
import              sys
import              argparse

from collections import defaultdict

from typing import Dict, List, Sequence

import fsl.get_standard as get_standard


__version__ = get_standard.__version__


def getStandard(imageType  : str   = None,
                resolution : float = None,
                modality   : str   = None) -> str:
    """Return a path to the requested standard template file. """
    manifest = get_standard.Manifest.getManifest()
    return manifest.get(imageType, modality, resolution)


def parseArgs(argv : Sequence[str] = None) -> argparse.Namespace:
    """Parses command-line arguments and returns an ``argparse.Namespace``
    object.
    """

    usage = 'fsl_get_standard [options] [image_type] [modality]'
    desc  = 'Generate paths to FSL standard space images'

    helps = {
        'version'    : 'Print version and exit.',
        'resolution' : 'Desired isotropic resolution in millimetres',
        'modality'   : 'Desired modality',
        'image_type' : 'Image type',
        'standard'   : 'Standard dataset (overrides $FSL_STANDARD)',
    }

    parser = argparse.ArgumentParser(
        'fsl_get_standard', usage=usage, description=desc)
    parser.add_argument(
        '-V', '--version', action='version', help=helps['version'],
        version=f'%(prog)s {get_standard.__version__}')
    parser.add_argument(
        'image_type', help=helps['image_type'], metavar='image_type',
        nargs='?')
    parser.add_argument(
        'modality', help=helps['modality'], metavar='modality', nargs='?')
    parser.add_argument(
        '-r', '--resolution', type=float, help=helps['resolution'])
    parser.add_argument(
        '-s', '--standard', help=helps['standard'])

    args = parser.parse_args(argv)

    return args


def main(argv : Sequence[str] = None):
    """Entry point for the ``fsl_get_standard`` script. Prints out the path
    to the requested standard template.
    """

    if 'FSLDIR' not in os.environ:
        print('The $FSLDIR environment variable is not set')
        return 1

    try:
        args     = parseArgs(argv)
        manifest = get_standard.Manifest.getManifest(args.standard)
        print(manifest.get(args.image_type, args.modality, args.resolution))

    except Exception as e:
        print(e, file=sys.stderr)
        return 1
    return 0


if __name__ == '__main__':
    sys.exit(main())
