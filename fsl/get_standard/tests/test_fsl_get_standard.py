#!/usr/bin/env python
#
# Unit tests for fsl_get_standard.py
#


import argparse
import contextlib as ctxlib
import io
import os
import os.path as op
import sys
import tempfile
import textwrap as tw

import pytest

from unittest import mock

import fsl.get_standard             as get_standard
import fsl.scripts.fsl_get_standard as cli


STANDARD_PATHS = [
    'data/standard/MNI152_T1_2mm.nii.gz',
    'data/standard/MNI152_T1_2mm_brain.nii.gz',
    'data/standard/MNI152_T1_2mm_brain_mask.nii.gz',
    'data/standard/MNI152_T1_1mm.nii.gz',
    'data/standard/MNI152_T1_1mm_brain.nii.gz',
    'data/standard/MNI152_T1_1mm_brain_mask.nii.gz',
    'data/standard/MNI152_T1_0.5mm.nii.gz',
    'data/standard/FMRIB58_FA_1mm.nii.gz',
    'data/standard/FMRIB58_FA-skeleton_1mm.nii.gz']

# ((imgtype, modality, resolution), expected result)
STANDARD_TESTS = [
    ((None,         None, None), 'MNI152_T1_2mm.nii.gz'),
    (('whole_head', None, None), 'MNI152_T1_2mm.nii.gz'),
    (('brain',      None, None), 'MNI152_T1_2mm_brain.nii.gz'),
    (('brain_mask', None, None), 'MNI152_T1_2mm_brain_mask.nii.gz'),
    (('whole_head', 'T1', None), 'MNI152_T1_2mm.nii.gz'),
    (('brain',      'T1', None), 'MNI152_T1_2mm_brain.nii.gz'),
    (('brain_mask', 'T1', None), 'MNI152_T1_2mm_brain_mask.nii.gz'),
    ((None,         None, 1),    'MNI152_T1_1mm.nii.gz'),
    (('whole_head', None, 1),    'MNI152_T1_1mm.nii.gz'),
    (('brain',      None, 1),    'MNI152_T1_1mm_brain.nii.gz'),
    (('brain_mask', None, 1),    'MNI152_T1_1mm_brain_mask.nii.gz'),
    (( None,        None, 0.5),  'MNI152_T1_0.5mm.nii.gz'),
    (('whole_head', None, 0.5),  'MNI152_T1_0.5mm.nii.gz'),
]


CUSTOM_PATHS = [
    'data/mouse/0.5mm/T1/head.nii.gz',
    'data/mouse/0.5mm/T1/brain.nii.gz',
    'data/mouse/0.5mm/T1/mask.nii.gz',
    'data/mouse/1mm/T1/head.nii.gz',
    'data/mouse/1mm/T1/brain.nii.gz',
    'data/mouse/1mm/T1/mask.nii.gz',
    'data/mouse/0.5mm/T2/head.nii.gz',
    'data/mouse/0.5mm/T2/brain.nii.gz',
    'data/mouse/0.5mm/T2/mask.nii.gz',
    'data/mouse/1mm/T2/head.nii.gz',
    'data/mouse/1mm/T2/brain.nii.gz',
    'data/mouse/1mm/T2/mask.nii.gz']

CUSTOM_MANIFEST = """
whole_head, 0.5, T1, 0.5mm/T1/head.nii.gz
brain,      0.5, T1, 0.5mm/T1/brain.nii.gz
brain_mask, 0.5, T1, 0.5mm/T1/mask.nii.gz
whole_head, 1,   T1, 1mm/T1/head.nii.gz
brain,      1,   T1, 1mm/T1/brain.nii.gz
brain_mask, 1,   T1, 1mm/T1/mask.nii.gz
whole_head, 0.5, T2, 0.5mm/T2/head.nii.gz
brain,      0.5, T2, 0.5mm/T2/brain.nii.gz
brain_mask, 0.5, T2, 0.5mm/T2/mask.nii.gz
whole_head, 1,   T2, 1mm/T2/head.nii.gz
brain,      1,   T2, 1mm/T2/brain.nii.gz
brain_mask, 1,   T2, 1mm/T2/mask.nii.gz
""".strip()

# ((imgtype, modality, resolution), expected result)
CUSTOM_TESTS = [
    ((None,         None, None), '0.5mm/T1/head.nii.gz'),
    (('whole_head', None, None), '0.5mm/T1/head.nii.gz'),
    (('brain',      None, None), '0.5mm/T1/brain.nii.gz'),
    (('brain_mask', None, None), '0.5mm/T1/mask.nii.gz'),
    (('whole_head', None, 0.5),  '0.5mm/T1/head.nii.gz'),
    (('brain',      None, 0.5),  '0.5mm/T1/brain.nii.gz'),
    (('brain_mask', None, 0.5),  '0.5mm/T1/mask.nii.gz'),
    ((None,         None, 1),    '1mm/T1/head.nii.gz'),
    (('whole_head', None, 1),    '1mm/T1/head.nii.gz'),
    (('brain',      None, 1),    '1mm/T1/brain.nii.gz'),
    (('brain_mask', None, 1),    '1mm/T1/mask.nii.gz'),
    (('whole_head', 'T1', None), '0.5mm/T1/head.nii.gz'),
    (('brain',      'T1', None), '0.5mm/T1/brain.nii.gz'),
    (('brain_mask', 'T1', None), '0.5mm/T1/mask.nii.gz'),
    (('whole_head', 'T1', 0.5),  '0.5mm/T1/head.nii.gz'),
    (('brain',      'T1', 0.5),  '0.5mm/T1/brain.nii.gz'),
    (('brain_mask', 'T1', 0.5),  '0.5mm/T1/mask.nii.gz'),
    (('whole_head', 'T1', 1),    '1mm/T1/head.nii.gz'),
    (('brain',      'T1', 1),    '1mm/T1/brain.nii.gz'),
    (('brain_mask', 'T1', 1),    '1mm/T1/mask.nii.gz'),
    (('whole_head', 'T2', None), '0.5mm/T2/head.nii.gz'),
    (('brain',      'T2', None), '0.5mm/T2/brain.nii.gz'),
    (('brain_mask', 'T2', None), '0.5mm/T2/mask.nii.gz'),
    (('whole_head', 'T2', 0.5),  '0.5mm/T2/head.nii.gz'),
    (('brain',      'T2', 0.5),  '0.5mm/T2/brain.nii.gz'),
    (('brain_mask', 'T2', 0.5),  '0.5mm/T2/mask.nii.gz'),
    (('whole_head', 'T2', 1),    '1mm/T2/head.nii.gz'),
    (('brain',      'T2', 1),    '1mm/T2/brain.nii.gz'),
    (('brain_mask', 'T2', 1),    '1mm/T2/mask.nii.gz'),
]


@ctxlib.contextmanager
def test_dir(contents=None):
    """Context manager to create, and change into, a temporary directory, and
    then afterwards delete it and change back to the original working
    directory.

    Contents may be a list of mock file paths, specified relative to the
    created directory - an empty file is created for each path.
    """

    prevdir = os.getcwd()

    with tempfile.TemporaryDirectory() as td:

        os.chdir(td)

        if contents is not None:
            for path in contents:
                os.makedirs(op.join(td, op.dirname(path)), exist_ok=True)
                with open(op.join(td, path), 'wt'):
                    pass

        try:
            yield td
        finally:
            os.chdir(prevdir)


@ctxlib.contextmanager
def mock_fsldir(contents=None, **envvars):
    """Context manager to create, and change into, a temporary directory, and
    then afterwards delete it and change back to the original working
    directory.

    The FSLDIR environment variable is set to the directory path. Any envars
    that are provided are also set.

    Contents may be a list of mock file paths, specified relative to the
    created directory - an empty file is created for each path.
    """
    env = os.environ.copy()
    env.update(envvars)

    with test_dir(contents) as td:
        env['FSLDIR'] = td
        with mock.patch.dict(os.environ, env, clear=True):
            yield td


def getstdout(func, *args, **kwargs):
    """Calls func, and captures anything printed to standard output.

    Returns the captured stdout, and the function return value.
    """
    stdout = io.StringIO()
    try:
        with mock.patch('sys.stdout', stdout):
            val = func(*args, **kwargs)
    except SystemExit as e:
        val = e.code
    return stdout.getvalue(), val


def test_default_templates_cli():

    with mock_fsldir(STANDARD_PATHS) as fsldir:
        stddir = op.join(fsldir, 'data', 'standard')

        for args, expected in STANDARD_TESTS:
            cliargs = []
            if args[0] is not None:
                cliargs.append(args[0])
            if args[1] is not None:
                cliargs.append(args[1])
            if args[2] is not None:
                cliargs.extend(('-r', str(args[2])))
            result, code = getstdout(cli.main, cliargs)
            assert code == 0
            assert result.strip() == op.join(stddir, expected)


def test_default_templates_manifest():

    with mock_fsldir(STANDARD_PATHS) as fsldir:
        stddir   = op.join(fsldir, 'data', 'standard')
        manifest = get_standard.Manifest.getManifest()

        for args, expected in STANDARD_TESTS:
            args = dict(zip(('imageType', 'modality', 'resolution'), args))
            assert manifest.get(   **args) == op.join(stddir, expected)
            assert cli.getStandard(**args) == op.join(stddir, expected)

        # Specifying modality without image type
        # is not supported on the command-line,
        # but should works with the API
        assert manifest.get(   None, 'T1', None) == op.join(stddir, 'MNI152_T1_2mm.nii.gz')
        assert manifest.get(   None, 'T1', 1)    == op.join(stddir, 'MNI152_T1_1mm.nii.gz')
        assert cli.getStandard(None, 1,    'T1') == op.join(stddir, 'MNI152_T1_1mm.nii.gz')
        assert cli.getStandard(None, None, 'T1') == op.join(stddir, 'MNI152_T1_2mm.nii.gz')

        assert getstdout(cli.main, ['', 'T1'])[1]            != 0
        assert getstdout(cli.main, ['', 'T1', '-r', '2'])[1] != 0

        assert sorted(manifest.imageTypes())               == sorted(['whole_head', 'brain', 'brain_mask', 'FA', 'FA_skeleton'])
        assert sorted(manifest.modalities())               == ['DTI', 'T1']
        assert sorted(manifest.modalities('whole_head'))   == ['T1']
        assert sorted(manifest.modalities('FA'))           == ['DTI']
        assert sorted(manifest.resolutions())              == [0.5, 1, 2]
        assert sorted(manifest.resolutions('whole_head'))  == [0.5, 1, 2]
        assert sorted(manifest.resolutions('brain'))       == [1, 2]
        assert sorted(manifest.resolutions('brain', 'T1')) == [1, 2]
        assert sorted(manifest.resolutions('brain', 'FA')) == []


def test_custom_templates_cli():
    with mock_fsldir(CUSTOM_PATHS, FSL_STANDARD='mouse') as fsldir:
        stddir = op.join(fsldir, 'data', 'mouse')
        with open(op.join(stddir, 'manifest.txt'), 'wt') as f:
            f.write(CUSTOM_MANIFEST)
        for args, expected in CUSTOM_TESTS:
            cliargs = []
            if args[0] is not None:
                cliargs.append(args[0])
            if args[1] is not None:
                cliargs.append(args[1])
            if args[2] is not None:
                cliargs.extend(('-r', str(args[2])))
            result, code = getstdout(cli.main, cliargs)
            assert code   == 0
            assert result.strip() == op.join(stddir, expected)


def test_custom_templates_manifest():
    with mock_fsldir(CUSTOM_PATHS, FSL_STANDARD='mouse') as fsldir:
        stddir = op.join(fsldir, 'data', 'mouse')
        with open(op.join(stddir, 'manifest.txt'), 'wt') as f:
            f.write(CUSTOM_MANIFEST)
        manifest = get_standard.Manifest.getManifest()

        for args, expected in CUSTOM_TESTS:
            args = dict(zip(('imageType', 'modality', 'resolution'), args))
            assert manifest.get(**args) == op.join(stddir, expected)

        # Specifying modality without image type
        # is not supported on the command-line,
        # but should works with the API
        assert manifest.get(   None, 'T1', None) == op.join(stddir, '0.5mm/T1/head.nii.gz')
        assert manifest.get(   None, 'T2', None) == op.join(stddir, '0.5mm/T2/head.nii.gz')
        assert manifest.get(   None, 'T1', 1)    == op.join(stddir, '1mm/T1/head.nii.gz')
        assert manifest.get(   None, 'T2', 1)    == op.join(stddir, '1mm/T2/head.nii.gz')
        assert cli.getStandard(None, None, 'T1') == op.join(stddir, '0.5mm/T1/head.nii.gz')
        assert cli.getStandard(None, None, 'T2') == op.join(stddir, '0.5mm/T2/head.nii.gz')
        assert cli.getStandard(None, 1,    'T1') == op.join(stddir, '1mm/T1/head.nii.gz')
        assert cli.getStandard(None, 1,    'T2') == op.join(stddir, '1mm/T2/head.nii.gz')

        assert getstdout(cli.main, ['', 'T1'])[1]            != 0
        assert getstdout(cli.main, ['', 'T1', '-r', '2'])[1] != 0
        assert getstdout(cli.main, ['', 'T2'])[1]            != 0
        assert getstdout(cli.main, ['', 'T2', '-r', '2'])[1] != 0

        assert sorted(manifest.imageTypes())  == sorted(['whole_head', 'brain', 'brain_mask'])
        assert sorted(manifest.modalities())  == ['T1', 'T2']
        assert sorted(manifest.resolutions()) == [0.5, 1]


# Test setting FSL_STANDARD to a sub-directory, e.g. to support different
# versions of a template, e.g.
#  - $FSLDIR/data/omm/Oxford-MM-1/
#  - $FSLDIR/data/omm/Oxford-MM-2/
def test_custom_templates_subdir_cli():
    paths = [f.replace('/mouse/', '/mouse/v1/') for f in CUSTOM_PATHS]
    with mock_fsldir(paths, FSL_STANDARD='mouse/v1') as fsldir:
        stddir = op.join(fsldir, 'data', 'mouse', 'v1')
        with open(op.join(stddir, 'manifest.txt'), 'wt') as f:
            f.write(CUSTOM_MANIFEST)
        for args, expected in CUSTOM_TESTS:
            cliargs = []
            if args[0] is not None:
                cliargs.append(args[0])
            if args[1] is not None:
                cliargs.append(args[1])
            if args[2] is not None:
                cliargs.extend(('-r', str(args[2])))
            result, code = getstdout(cli.main, cliargs)
            assert code   == 0
            assert result.strip() == op.join(stddir, expected)


def test_no_fsldir():
    env = os.environ.copy()
    env.pop('FSLDIR', None)
    with mock.patch.dict(os.environ, env, clear=True):
        with pytest.raises(KeyError):
            get_standard.Manifest.getManifest()
        assert cli.main([]) != 0


def test_manifest_missing():
    with mock_fsldir(CUSTOM_PATHS, FSL_STANDARD='mouse'):
        with pytest.raises(Exception):
            get_standard.Manifest.getManifest()
        assert cli.main([]) != 0


def test_unknown_image_type():
    with mock_fsldir(STANDARD_PATHS):
        manifest = get_standard.Manifest.getManifest()
        with pytest.raises(Exception):
            manifest.get('no_head')
        assert cli.main(['no_head']) != 0
    with mock_fsldir(CUSTOM_PATHS, FSL_STANDARD='mouse') as fsldir:
        stddir = op.join(fsldir, 'data', 'mouse')
        with open(op.join(stddir, 'manifest.txt'), 'wt') as f:
            f.write(CUSTOM_MANIFEST)
        with pytest.raises(Exception):
            manifest.get('no_head')

        assert cli.main(['no_head']) != 0


def test_unknown_resolution():
    with mock_fsldir(STANDARD_PATHS):
        manifest = get_standard.Manifest.getManifest()
        with pytest.raises(Exception):
            manifest.get('brain', 0.5)
        assert cli.main(['brain', '-r', '0.5']) != 0


def test_template_missing():
    # skip the first template
    with mock_fsldir(STANDARD_PATHS[1:]):
        with pytest.raises(Exception):
            manifest = get_standard.Manifest.getManifest()
        assert cli.main([]) != 0

    with mock_fsldir(CUSTOM_PATHS[1:], FSL_STANDARD='mouse') as fsldir:
        stddir   = op.join(fsldir, 'data', 'mouse')
        with open(op.join(stddir, 'manifest.txt'), 'wt') as f:
            f.write(CUSTOM_MANIFEST)

        # if a template file is missing,
        # an error is raised when the
        # manifest file gets loaded
        with pytest.raises(Exception):
            manifest = get_standard.Manifest.getManifest()

        assert cli.main([]) != 0


def test_manifest_sans_modality():
    paths = [
        'data/mouse/T1_head.nii.gz',
        'data/mouse/T1_brain.nii.gz',
        'data/mouse/T2_head.nii.gz',
        'data/mouse/T2_brain.nii.gz',
    ]
    manifest = tw.dedent("""
    # Also make sure comments and blank lines are handled

    whole_head, 1,     T1_head.nii.gz

    brain,      1,     T1_brain.nii.gz
    whole_head, 1, T2, T2_head.nii.gz
    brain,      1, T2, T2_brain.nii.gz
    """).strip()

    with mock_fsldir(paths, FSL_STANDARD='mouse') as fsldir:
        stddir = op.join(fsldir, 'data', 'mouse')
        with open(op.join(stddir, 'manifest.txt'), 'wt') as f:
            f.write(manifest)

        manifest = get_standard.Manifest.getManifest()

        assert sorted(manifest.imageTypes())  == ['brain', 'whole_head']
        assert manifest.modalities()          == [None, 'T2']
        assert sorted(manifest.resolutions()) == [1]

        assert manifest.get()                   == op.join(stddir, 'T1_head.nii.gz')
        assert manifest.get('whole_head')       == op.join(stddir, 'T1_head.nii.gz')
        assert manifest.get('brain')            == op.join(stddir, 'T1_brain.nii.gz')
        assert manifest.get(None,         'T2') == op.join(stddir, 'T2_head.nii.gz')
        assert manifest.get('whole_head', 'T2') == op.join(stddir, 'T2_head.nii.gz')
        assert manifest.get('brain',      'T2') == op.join(stddir, 'T2_brain.nii.gz')


def test_malformed_manifest():
    badmanifests = [
        "not,enough",
        "too,many,columns,on,this,line",
        "bad,resolution_value,image.nii.gz",
        "bad,resolution_value,again,image.nii.gz",
        "non,existent,path",
        "another,non,existent,path",
    ]

    for content in badmanifests:
        with mock_fsldir(['data/mouse/image.nii.gz'], FSL_STANDARD='mouse') as fsldir:
            stddir = op.join(fsldir, 'data', 'mouse')
            with open(op.join(stddir, 'manifest.txt'), 'wt') as f:
                f.write(content)

            with pytest.raises(Exception):
                get_standard.Manifest.getManifest()


# custom manifests may contain image types other than
# "whole_head", "brain", "brain_mask"
def test_custom_image_type():
    manifest = tw.dedent("""
    whole_head, 1, T1, T1_head.nii.gz
    banana, 1, T1, T1_banana.nii.gz
    """).strip()

    contents = [
        'data/mouse/T1_head.nii.gz',
        'data/mouse/T1_banana.nii.gz',
    ]

    with mock_fsldir(contents, FSL_STANDARD='mouse') as fsldir:
        stddir = op.join(fsldir, 'data', 'mouse')
        with open(op.join(stddir, 'manifest.txt'), 'wt') as f:
            f.write(manifest)

        manifest = get_standard.Manifest.getManifest()

        assert sorted(manifest.imageTypes())  == ['banana', 'whole_head']
        assert manifest.modalities()          == ['T1']
        assert sorted(manifest.resolutions()) == [1]
        assert manifest.get()                   == op.join(stddir, 'T1_head.nii.gz')
        assert manifest.get('whole_head')       == op.join(stddir, 'T1_head.nii.gz')
        assert manifest.get('banana')           == op.join(stddir, 'T1_banana.nii.gz')



# Test setting FSL_STANDARD to a directory outside of FSLDIR
def test_custom_templates_in_external_dir_cli():

    with test_dir(CUSTOM_PATHS) as tempdir:
        stddir = op.join(tempdir, 'data', 'mouse')
        with mock_fsldir(FSL_STANDARD=stddir) as fsldir:
            with open(op.join(stddir, 'manifest.txt'), 'wt') as f:
                f.write(CUSTOM_MANIFEST)

            for args, expected in CUSTOM_TESTS:
                cliargs = []
                if args[0] is not None:
                    cliargs.append(args[0])
                if args[1] is not None:
                    cliargs.append(args[1])
                if args[2] is not None:
                    cliargs.extend(('-r', str(args[2])))
                result, code = getstdout(cli.main, cliargs)
                assert code   == 0
                assert result.strip() == op.join(stddir, expected)


# Test setting FSL_STANDARD to a directory outside of FSLDIR
def test_custom_templates_in_external_dir_manifest():

    with test_dir(CUSTOM_PATHS) as tempdir:
        stddir = op.join(tempdir, 'data', 'mouse')
        with mock_fsldir(FSL_STANDARD=stddir) as fsldir:
            with open(op.join(stddir, 'manifest.txt'), 'wt') as f:
                f.write(CUSTOM_MANIFEST)
            manifest = get_standard.Manifest.getManifest()

            for args, expected in CUSTOM_TESTS:
                args = dict(zip(('imageType', 'modality', 'resolution'), args))
                assert manifest.get(**args) == op.join(stddir, expected)

            # Specifying modality without image type
            # is not supported on the command-line,
            # but should works with the API
            assert manifest.get(   None, 'T1', None) == op.join(stddir, '0.5mm/T1/head.nii.gz')
            assert manifest.get(   None, 'T2', None) == op.join(stddir, '0.5mm/T2/head.nii.gz')
            assert manifest.get(   None, 'T1', 1)    == op.join(stddir, '1mm/T1/head.nii.gz')
            assert manifest.get(   None, 'T2', 1)    == op.join(stddir, '1mm/T2/head.nii.gz')
            assert cli.getStandard(None, None, 'T1') == op.join(stddir, '0.5mm/T1/head.nii.gz')
            assert cli.getStandard(None, None, 'T2') == op.join(stddir, '0.5mm/T2/head.nii.gz')
            assert cli.getStandard(None, 1,    'T1') == op.join(stddir, '1mm/T1/head.nii.gz')
            assert cli.getStandard(None, 1,    'T2') == op.join(stddir, '1mm/T2/head.nii.gz')

            assert getstdout(cli.main, ['', 'T1'])[1]            != 0
            assert getstdout(cli.main, ['', 'T1', '-r', '2'])[1] != 0
            assert getstdout(cli.main, ['', 'T2'])[1]            != 0
            assert getstdout(cli.main, ['', 'T2', '-r', '2'])[1] != 0

            assert sorted(manifest.imageTypes())  == sorted(['whole_head', 'brain', 'brain_mask'])
            assert sorted(manifest.modalities())  == ['T1', 'T2']
            assert sorted(manifest.resolutions()) == [0.5, 1]


# Test setting FSL_STANDARD via the --standard cli option
def test_custom_templates_in_external_dir_standard_cli_option():

    with test_dir(CUSTOM_PATHS) as tempdir:
        stddir = op.join(tempdir, 'data', 'mouse')
        with mock_fsldir() as fsldir:
            with open(op.join(stddir, 'manifest.txt'), 'wt') as f:
                f.write(CUSTOM_MANIFEST)

            for args, expected in CUSTOM_TESTS:
                cliargs = ['--standard', stddir]
                if args[0] is not None:
                    cliargs.append(args[0])
                if args[1] is not None:
                    cliargs.append(args[1])
                if args[2] is not None:
                    cliargs.extend(('-r', str(args[2])))
                result, code = getstdout(cli.main, cliargs)
                assert code   == 0
                assert result.strip() == op.join(stddir, expected)


if __name__ == '__main__':

    success = True
    tests   = []
    thismod = sys.modules[__name__]

    for testname in dir(thismod):
        if testname.startswith('test_'):
            test = getattr(thismod, testname)
            if callable(test):
                tests.append(test)

    for test in tests:
        try:
            test()
            print(f'{test.__name__} PASS')
        except Exception:
            print(f'{test.__name__} FAIL')
            success = False
    sys.exit(0 if success else 1)
