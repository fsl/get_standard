#!/usr/bin/env python
#
# fsl.get_standard - Generate paths to FSL standard space images.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#
"""The fsl.get_standard module can be used to locate standard template files,
e.g. $FSLDIR/data/standard/MNI152_T1_2mm.nii.gz.  It is intended to be used by
tools which can be used with standard templates other than the MNI152.
"""


import os.path   as op
import              os

from collections import defaultdict

from typing import List


__version__ = '0.5.1'
"""fsl_get_standard version number."""


class Manifest:
    """Simple container class for storing template file paths. """


    @staticmethod
    def getManifest(stddir : str = None) -> 'Manifest':
        """Loads and returns a template manifest. The stddir argument
        can be a FSL standard dataset identifier (the name of a directory
        within $FSLDIR/data/), or the path to a FSL standard dataset.

        If stddir is not provided, it is set to the $FSL_STANDARD environment
        variable. If the $FSL_STANDARD environment variable is not set, or
        is set to "standard", "mni152", or "MNI152", the manifest for
        $FSLDIR/data/standard/ (the MNI152 templates) is returned.
        """

        fsldir = os.environ['FSLDIR']

        if stddir is None:
            stddir = os.environ.get('FSL_STANDARD')

        # Select default manifest
        if stddir in (None, 'MNI152', 'mni152', 'standard'):
            return Manifest.defaultManifest()

        # FSL_STANDARD may be an identifier for a
        # dataset in ${FSLDIR}/data/${FSL_STANDARD}/
        if not op.exists(stddir):
            stddir = op.join(fsldir, 'data', stddir)

        # Or may be set to any directory containing
        # a standard template data set.
        else:
            stddir = op.abspath(stddir)

        manifestFile = op.join(stddir, 'manifest.txt')

        if not op.exists(manifestFile):
            raise Exception(f'Manifest file missing: {manifestFile}')

        return Manifest.loadManifestFile(manifestFile)


    @staticmethod
    def defaultManifest() -> 'Manifest':
        """Called by Manifest.getManifest when the FSL_STANDARD variable is
        not set.

        Returns the manifest for the default FSL MNI152 standard template
        images, contained in $FSLDIR/data/standard/.
        """

        fsldir    = os.environ['FSLDIR']
        stddir    = op.join(fsldir, 'data', 'standard')
        manifest  = Manifest()
        filepaths = [
            ('whole_head',  'T1',  2,   op.join(stddir, 'MNI152_T1_2mm.nii.gz')),
            ('whole_head',  'T1',  1,   op.join(stddir, 'MNI152_T1_1mm.nii.gz')),
            ('whole_head',  'T1',  0.5, op.join(stddir, 'MNI152_T1_0.5mm.nii.gz')),
            ('brain',       'T1',  2,   op.join(stddir, 'MNI152_T1_2mm_brain.nii.gz')),
            ('brain',       'T1',  1,   op.join(stddir, 'MNI152_T1_1mm_brain.nii.gz')),
            ('brain_mask',  'T1',  2,   op.join(stddir, 'MNI152_T1_2mm_brain_mask.nii.gz')),
            ('brain_mask',  'T1',  1,   op.join(stddir, 'MNI152_T1_1mm_brain_mask.nii.gz')),
            ('FA',          'DTI', 1,   op.join(stddir, 'FMRIB58_FA_1mm.nii.gz')),
            ('FA_skeleton', 'DTI', 1,   op.join(stddir, 'FMRIB58_FA-skeleton_1mm.nii.gz')),
        ]

        for imgtype, modality, resolution, filepath in filepaths:
            manifest.add(imgtype, modality, resolution, filepath)

        return manifest


    @staticmethod
    def loadManifestFile(manifestFile : str) -> 'Manifest':
        """Called by Manifest.getManifest. Loads and returns a manifest from a
        manifest file.
        """

        manifest = defaultdict(dict)
        dirname  = op.dirname(op.abspath(manifestFile))

        with open(manifestFile, 'rt') as f:
            lines = f.readlines()

        lines = [l.strip() for l in lines]
        lines = [l         for l in lines if not (l == '' or l.startswith('#'))]

        manifest = Manifest()

        for line in lines:

            try:
                words = line.split(',')

                # <= 0.2.0 - type, resolution, filepath
                if len(words) == 3:
                    imgtype    =       words[0].strip()
                    resolution = float(words[1].strip())
                    modality   =       None
                    fpath      =       words[2].strip()
                # >= 0.3.0 - type, resolution, modality, filepath
                elif len(words) == 4:
                    imgtype    =       words[0].strip()
                    resolution = float(words[1].strip())
                    modality   =       words[2].strip()
                    fpath      =       words[3].strip()
                else:
                    raise ValueError('Invalid line in manifest '
                                     f'{manifestFile}: {line}')

                fpath = op.join(dirname, fpath)

                if not op.exists(fpath):
                    raise ValueError(f'Cannot find file: {fpath}')

            except Exception as e:
                raise Exception('There is an issue with the FSL standard '
                                f'template manifest file {manifestFile}: '
                                f'{str(e)} (line: {line})')

            manifest.add(imgtype, modality, resolution, fpath)

        return manifest


    def __init__(self):
        """Don't create a Manifest object - use the Manifest.getManifest method
        instead.
        """

        self.__manifest = {}


    def add(self,
            imageType  : str,
            modality   : str,
            resolution : float,
            filepath   : str):
        """Add a template file to the manifest. """

        man   = self.__manifest
        itype = imageType
        mod   = modality
        res   = resolution

        if not op.exists(filepath):
            raise Exception(f'Template file does not exist: {filepath}')

        if itype not in man:             man[itype]           = {}
        if mod   not in man[itype]:      man[itype][mod]      = {}
        if res   not in man[itype][mod]: man[itype][mod][res] = {}

        man[itype][mod][res] = filepath


    def get(self,
            imageType  : str   = None,
            modality   : str   = None,
            resolution : float = None) -> str:
        """Retrieve a template file path with the specified properties. """

        manifest = self.__manifest

        if imageType is None:
            imageType = list(manifest.keys())[0]

        if imageType not in manifest:
            raise Exception(f'No template files with image type: {imageType}')

        manifest = manifest[imageType]

        if modality is None:
            modality = list(manifest.keys())[0]

        if modality not in manifest:
            raise Exception(f'No template files of image type {imageType} '
                            f'with modality: {modality}')

        manifest = manifest[modality]

        if resolution is None:
            resolution = list(manifest.keys())[0]

        if resolution not in manifest:
            raise Exception(f'No template files of image type {imageType} '
                            f'and modality {modality} with '
                            f'resolution: {resolution}')

        return manifest[resolution]


    def imageTypes(self) -> List[str]:
        """Return a list of all image types in the manifest. """
        return list(self.__manifest.keys())


    def modalities(self, imageType : str = None) -> List[str]:
        """Return a list of all modalities in the manifest. If
        imageType is provided, returns only those modalities that are
        present for imageType.
        """

        manifest   = self.__manifest
        modalities = []

        if imageType is not None: imageTypes = [imageType]
        else:                     imageTypes = self.imageTypes()

        for imageType in imageTypes:
            if imageType not in manifest:
                continue
            for modality in manifest[imageType].keys():
                if modality not in modalities:
                    modalities.append(modality)

        return modalities


    def resolutions(self,
                    imageType : str = None,
                    modality  : str = None) -> List[float]:
        """Return a list of all resolutions in the manifest.  If imageType
        and/or modality are provided, returns only those resolutions that
        are present for imageType/modality.
        """

        manifest    = self.__manifest
        resolutions = []

        if imageType is not None: imageTypes = [imageType]
        else:                     imageTypes = self.imageTypes()

        for imageType in imageTypes:

            if imageType not in manifest:
                continue

            if modality is not None:  modalities = [modality]
            else:                     modalities = self.modalities(imageType)

            for modality in modalities:
                if modality not in manifest[imageType]:
                    continue

                for resolution in manifest[imageType][modality].keys():
                    if resolution not in resolutions:
                        resolutions.append(resolution)

        return resolutions
