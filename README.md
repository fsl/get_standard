# `fsl_get_standard`


The `fsl_get_standard` command can be used to locate standard-space template images within `$FSLDIR`. It is intended to be used by tools which can be used with standard templates other than the MNI152.


The intent of the `fsl_get_standard` tool is to allow a script/program to be written without being tightly coupled to a specific set of standard template files, such as the MNI152. Instead of hard-coding paths to specific template files, a script can call (e.g.) `fsl_get_standard brain T1` to get a skull-stripped T1 image from the standard template dataset that the user has requested.


## Usage

```
fsl_get_standard [<image_type>] \
    [<modality>]                \
    [-r <resolution>]           \
    [-s <standard>]
```

where:

 - `<image_type>` is one of:
   - `whole_head`
   - `brain`
   - `brain_mask`

 - `<resolution>` is desired isotropic resolution in mm, e.g. `2`, `0.5`, etc

 - `<modality>` is the image modality, for template datasets which contain images for multiple modalities, e.g. `T1`, `T2`, etc.

 - `<standard>` is the standard template dataset identifier, the name of a sub-directory within `$FSLDIR/data/`. This can also be specified by setting the `$FSL_STANDARD` environment variable (see _Selecting standard templates_, below).


If a requested template file does not exist, `fsl_get_standard` will print an error, and return a non-zero exit code.

## Examples


```
fsl_get_standard             # /usr/local/fsl/data/standard/MNI152_T1_2mm.nii.gz
fsl_get_standard whole_head  # /usr/local/fsl/data/standard/MNI152_T1_2mm.nii.gz
fsl_get_standard brain_mask  # /usr/local/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz
fsl_get_standard brain -r 1  # /usr/local/fsl/data/standard/MNI152_T1_1mm_brain.nii.gz

# Use a different set of standard templates
export FSL_STANDARD=omm/Oxford-MM-1
fsl_get_standard                # /usr/local/fsl/data/omm/Oxford-MM-1/OMM-1_T1_head.nii.gz
fsl_get_standard whole_head FA  # /usr/local/fsl/data/omm/Oxford-MM-1/OMM-1_DTI_FA.nii.gz
```

## Selecting standard templates

All standard templates that are a part of FSL are located within the `$FSLDIR/data/` directory. The MNI152 standard templates are located within `$FSLDIR/data/standard/`. Other templates may be located within other sub-directories, e.g. `$FSLDIR/data/omm/Oxford-MM-1/`, `$FSLDIR/data/mouse/`, etc.

The `fsl_get_standard` tool reads an environment variable called `$FSL_STANDARD`, which specifies the standard templates that should be used. If `$FSL_STANDARD` is not set, the MNI152 templates are used.  Otherwise, the templates within `$FSLDIR/data/$FSL_STANDARD/` are used.

> The default MNI152 templates will also be selected if `$FSL_STANDARD` is set to any of `mni152`, `MNI152`, or `standard`.

The `$FSL_STANDARD` variable may also be set to a directory located outside of `$FSLDIR`, which contains standard template files, e.g. `FSL_STANDARD=/home/user/my_data/my_standard/`. This directory must be organised as described in _Creating a template dataset_, below.

> As an alternative to using the `$FSL_STANDARD` environment variable, the standard templates to use can also be specified at the command-line via the `-s`/`--standard` flag.


## Creating a template dataset

If you are developing a set of standard templates for use with FSL, you need to create a `manifest.txt` file which describes the template files, so that they can be found by `fsl_get_standard`.

Within each template directory is a file called `manifest.txt`, which is a comma-separated text file containing the images that are provided in the directory. The `manifest.txt` file contains one line for each file, and three or four columns, specifying the file type, resolution, modality (optional), and file name. For example, the standard MNI152 templates in `$FSLDIR/data/standard/` would be described by a `manifest.txt` file containing:


```
whole_head,  2,   T1,  MNI152_T1_2mm.nii.gz
brain,       2,   T1,  MNI152_T1_2mm_brain.nii.gz
brain_mask,  2,   T1,  MNI152_T1_2mm_brain_mask.nii.gz
whole_head,  1,   T1,  MNI152_T1_1mm.nii.gz
brain,       1,   T1,  MNI152_T1_1mm_brain.nii.gz
brain_mask,  1,   T1,  MNI152_T1_1mm_brain_mask.nii.gz
whole_head,  0.5, T1,  MNI152_T1_0.5mm.nii.gz
FA,          1,   DTI, FMRIB58_FA_1mm.nii.gz
FA_skeleton, 1,   DTI, FMRIB58_FA-skeleton_1mm.nii.gz
```


An example `manifest.txt` file for a set of templates with different modalities might look like:


```
whole_head, 2, T1, mytemplate_T1_2mm.nii.gz
brain,      2, T1, mytemplate_T1_2mm_brain.nii.gz
brain_mask, 2, T1, mytemplate_T1_2mm_brain_mask.nii.gz
whole_head, 2, T2, mytemplate_T2_2mm.nii.gz
brain,      2, T2, mytemplate_T2_2mm_brain.nii.gz
brain_mask, 2, T2, mytemplate_T2_2mm_brain_mask.nii.gz
```


Blank lines, and lines beginning with a `#` character, are ignored.

The order of lines within `manifest.txt` defines the default values for the image type, modality, and resolution. If an image type is not specified, the first image type in `manifest.txt` is used.

Similarly, if `fsl_get_standard` is called without a modality or resolution being specified, the *first* modality (if defined) and resolution in `manifest.txt`, which matches the requested image type, will be used as the default.

Template file names are specified relative to the location of `manifest.txt` (i.e. the template directory) using UNIX-style relative paths. For example, if a template collection called `mouse` contains the following directory structure:


```
$FSLDIR/data/mouse
├── manifest.txt
├── 0.5mm
│   ├── brain.nii.gz
│   ├── head.nii.gz
│   └── mask.nii.gz
└── 1mm
    ├── brain.nii.gz
    ├── head.nii.gz
    └── mask.nii.gz
```


The `manifest.txt` file would contain:


```
whole_head, 0.5, 0.5mm/head.nii.gz
brain,      0.5, 0.5mm/brain.nii.gz
brain_mask, 0.5, 0.5mm/mask.nii.gz
whole_head, 1,   1mm/head.nii.gz
brain,      1,   1mm/brain.nii.gz
brain_mask, 1,   1mm/mask.nii.gz
```

### Image type and modality identifiers

Remember that the intent of `fsl_get_standard` is to allow a script/program to be written without hard-coding paths to (e.g.) MNI152 template files, and instead to allow the user to select the templates they wish to use. A script may therefore make calls such as `fsl_get_standard brain`, `fsl_get_standard whole_head`, etc, under the assumption that template files for `brain`, `whole_head` etc. exist.

When creating a `manifest.txt` file, it is therefore important to use appropriate identifiers for the modality and image type. While there are no strict rules, the following conventions are in use and are recommended.

Identifiers for modalities:
 - `T1` - T1-weighted MRI
 - `T2` - T2-weighted MRI
 - `T2_FLAIR`- T2-FLAIR-weighted MRI
 - `DTI`- Diffusion tensor metrics (e.g. as generated by `dtifit`)

Image types to be used with any modality:

 - `whole_head` - whole head MRI (i.e. not skull-stripped).
 - `brain` - brain-extracted MRI
 - `brain_mask` - binary or weighted brain mask

Image types to be used with the `DTI` modality:
 - `FA` - fractional anisotropy
 - `FA_skeleton` - fractional anisotropy skeleton (for use with TBSS)
 - `V1`/`V2`/`V3`/`L1`/`L`/`L3`/`MD`/`M0`/`tensor`- Other derived DTI metrics


## Development


The `fsl_get_standard` source code is hosted at https://git.fmrib.ox.ac.uk/fsl/get_standard.

When preparing a new version of `fsl_get_standard`, remember to update the version number in `fsl/get_standard_/__init__.py`. Then create a new tag at the above git repository.
